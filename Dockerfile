FROM ubuntu:20.10
 
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update
RUN apt-get -y install python3 python3-nacl python3-pip libffi-dev
RUN pip3 install ansible

RUN apt-get -y install openssh-server

COPY ansible/* /root/ansible/  
WORKDIR /root/ansible/

CMD ansible-playbook -i /root/ansible/hosts install.yml
