BEFORE RUNNING THIS SCRIPT, please READ THIS INFORMATION
=========

> **WARNING!!! Installation of `traefik` doesn't work.**

What does it do
----

This `Dockerfile` runs playbook `install.yml` that runs installation of `traefik`, `gitlab-runner`, `docker`, `postgres` and `nginx`

Running Dockerfile
----

Before running `Dockerfile` you need to customize the installation:
- You need to change in the ansible/install.yml the line `remote_user:` `novenkij` to your username.
- You need to choose which programs you need to install. For example, if you don't want to install traefik, you need to delete line `- traefik`.
- You need to change in the ansible/hosts `192.168.56.5` to ip of your server and `novenkij` to user of your server.

To run Dockerfile use: 
`$ docker build -t ansible-install .`
